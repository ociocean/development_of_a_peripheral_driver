#include "timer.h"
#include <xc.h> //Because this file has to be compiled with the right flags; jst with the c compiler

#ifndef PBUSCLK
#warning "PBUSCLK unset! Please make sure that you set PBUSCLK proper. For now PBUSCLK will be set to 40000000L"
#define PBUSCLK 40000000L
#endif

#define TIMER_DEBUG 0

// HIDDEN DEFINES THE USER DON'T NEED TO SEE.
#define MAX_PRESCALER 256
#define TPS_256 7 // TCKPS code for xx pre-scaler
#define TPS_64  6
#define TPS_32  5
#define TPS_16  4
#define TPS_8   3
#define TPS_4   2
#define TPS_2   1
#define TPS_1   0
int get_prescalerBits(int prescaler);
int get_TxIE_bitShift(int timer);
int get_TxIF_bitShift(int timer);
int get_TxIP_bitShift(int timer);

#define NUM_SUPPORTED_TIMERS 5
volatile unsigned int* pTimerConfigurationBits[NUM_SUPPORTED_TIMERS] = {&T1CON, &T2CON, &T3CON, &T4CON, &T5CON};
volatile unsigned int* pTimer[NUM_SUPPORTED_TIMERS] = {&TMR1, &TMR2, &TMR3, &TMR4, &TMR5};
volatile unsigned int* pPRx[NUM_SUPPORTED_TIMERS] = {&PR1, &PR2, &PR3, &PR4, &PR5};
volatile unsigned int* pIPCx[NUM_SUPPORTED_TIMERS] = {&IPC1, &IPC2, &IPC3, &IPC4, &IPC5};

#define NUM_SUPPORTED_OUTPUT_COMPARE 5
volatile unsigned int* pOCxCON[NUM_SUPPORTED_OUTPUT_COMPARE] = {&OC1CON, &OC2CON, &OC3CON, &OC4CON, &OC5CON};
volatile unsigned int* pOCxRS[NUM_SUPPORTED_OUTPUT_COMPARE] = {&OC1RS, &OC2RS, &OC3RS, &OC4RS, &OC5RS}; 

#define NUM_POSSIBLE_PRESCALER 8
unsigned int possible_prescaler[NUM_POSSIBLE_PRESCALER] = {1, 2, 4, 8, 16, 32, 64, 256};


int stop_timer(int timer){
	if(timer<1 || timer>MAX_SUPPORTED_TIMERS){
		return ERROR_UNSOPPORTED_INPUT;
	}
	*pTimerConfigurationBits[timer -1] &= ~(1<<15); //Clear the bit
	return SUCCESS;
}

int start_timer(int timer){
	if(timer<1 || timer>MAX_SUPPORTED_TIMERS){
		return ERROR_UNSOPPORTED_INPUT;
	}
	*pTimerConfigurationBits[timer -1] |= (1<<15); //Set the bit
	return SUCCESS;
}

int set_timerPrescaler(int timer, int prescaler){
	if(timer<1 || timer>MAX_SUPPORTED_TIMERS){
		return ERROR_UNSOPPORTED_INPUT;
	}
	
	if(prescaler == 1 || prescaler == 2 || prescaler == 4 || prescaler == 8||prescaler == 16 
	|| prescaler == 32 || prescaler == 64 || prescaler == 256){
		//CLEAR PRESCALER BITS SO THEY CAN BE REWRITTEN:
		*pTimerConfigurationBits[timer-1] &= ~( (1<<4) | (1<<5) | (1<<6) );
		//WRITE PRESCALER BITS:
		*pTimerConfigurationBits[timer-1] |= (get_prescalerBits(prescaler)<<4);
	}
	return SUCCESS;
}

int get_prescalerBits(int prescaler){
	switch(prescaler){
		case 1:
			return TPS_1;
		case 2:
			return TPS_2;
		case 4:
			return TPS_4;
		case 8:
			return TPS_8;
		case 16:
			return TPS_16;
		case 32:
			return TPS_32;
		case 64:
			return TPS_64;
		case 256:
			return TPS_256;
		default:
			return 0;
	}
}

int set_timerPeriod(int timer, double period_time){
	if(timer<1 || timer>MAX_SUPPORTED_TIMERS){
		return ERROR_UNSOPPORTED_INPUT;
	}
	if(period_time>0){
		return ERROR_UNSOPPORTED_INPUT;
	}
	//CHECK IF PERIOD CAN BE REACHED WITH AN 16BIT TIMER:
	double max_16bit_timer_period;
	max_16bit_timer_period = (double)65536*MAX_PRESCALER/PBUSCLK;
	#if TIMER_DEBUG==1
		printf("set_timerPeriod: 16 bit timer is enough?: period_time==%f, max_16bit_timer_period==%f.\n", period_time, max_16bit_timer_period);
	#endif
	if(period_time<=max_16bit_timer_period){
		//FIND THE SMALLEST POSSIBLE PRESCALER FOR BEST ACCURACY:
		int i; 
		unsigned int prx;
		for(i=0; i<NUM_POSSIBLE_PRESCALER; i++){
			if((double)65536*possible_prescaler[i]/PBUSCLK>= period_time){
				//PRESCALER FOUND -> NOW CALCULATE THE BEST PRX VALUE AND LEAVE THE FOR LOOP:
				prx = period_time/possible_prescaler[i]*PBUSCLK;
				//SET TIMER CONFIGURATIONS:
				#if TIMER_DEBUG==1
					printf("set_timerPeriod: prescaler=%d, PRx=%d\n", possible_prescaler[i], prx);
				#endif
				set_timerPrescaler(timer, possible_prescaler[i]);
				*pPRx[timer-1] = prx;
				return SUCCESS;
			}
		}

	}
	if(timer%2==1){
		return ERROR_32BIT_TIMER_NEEDED;
	}
	//CHECK IF PERIOD CAN BE REACHED WITH AN 32BIT TIMER:
	/** \problem Since the PRx register is simulated as an unsigned int and not as long,
		32bit PRx values can not be set, thus the setting of 32bit timer here is not usefule.
		Because of this 32bit timer will not be supported.*/ 
/*
	double max_32bit_timer_period;
	max_32bit_timer_period = (double)(2^32)*MAX_PRESCALER/PBUSCLK;
	#if TIMER_DEBUG==1
		printf("set_timerPeriod: 16 bit timer is enough?: period_time==%f, max_16bit_timer_period==%f.\n", period_time, max_16bit_timer_period);
	#endif
	if(period_time<=max_32bit_timer_period){
		//FIND THE SMALLEST POSSIBLE PRESCALER FOR BEST ACCURACY:
		int i; 
		unsigned int prx; 
		for(i=0; i<NUM_POSSIBLE_PRESCALER; i++){
			if((double)(2^32)*possible_prescaler[i]/PBUSCLK>= period_time){
				//PRESCALER FOUND -> NOW CALCULATE THE BEST PRX VALUE AND LEAVE THE FOR LOOP:
				prx = period_time/possible_prescaler[i]*PBUSCLK;
				//SET TIMER CONFIGURATIONS:
				#if TIMER_DEBUG==1
					printf("set_timerPeriod: prescaler=%d, PRx=%d\n", possible_prescaler[i], prx);
				#endif
				set_timerPrescaler(timer, possible_prescaler[i]);
				*pPRx[timer-1] = prx;
				return SUCCESS;
			}
		}

	}
 */
	return ERROR_SETTING_NOT_POSSIBLE;
}

int set_timerFrequency(int timer, double frequency){
	if(timer<1 || timer>MAX_SUPPORTED_TIMERS){
		return ERROR_UNSOPPORTED_INPUT;
	}
	if(frequency<=0){
		return ERROR_UNSOPPORTED_INPUT;
	}
	return set_timerPeriod(timer, 1/frequency);
}

unsigned int read_timer(int timer){
	return *pTimer[timer-1];
}

int set_timerInterrupt(int timer, int on, int priority){
	if(timer<1 || timer>MAX_SUPPORTED_TIMERS){
		return ERROR_UNSOPPORTED_INPUT;
	}
	if(priority<1 || priority>6){
		return ERROR_UNSOPPORTED_INPUT;
	}
	
	if(on){
		//CLEAR IPCx PRIORITY BITS SO THEY CAN BE REWRITTEN:
		*pIPCx[timer-1] &= ~( (1<<get_TxIP_bitShift(timer)) | (1<<get_TxIP_bitShift(timer)+1) | (1<<get_TxIP_bitShift(timer)+2));
		//SET REGISTER
		IFS0 &= ~(1<<get_TxIF_bitShift(timer)); //Clear interrpt flag.
		*pIPCx[timer-1] = (priority<<get_TxIP_bitShift(timer));
		IEC0 |= (1<<get_TxIE_bitShift(timer)); //Enable interrupts.
		#warning "You need to run 'void __attribute__( (interrupt(IPL<PRIORITY>AUTO), vector(_TIMER_<TIMER>_VECTOR))) tmr<TIMER>_isr(void);' manually since this is not supporte in this library." 
	}
	else{
		IEC0 &= ~(1<<get_TxIE_bitShift(timer)); //Disable interrupts.
	}
	return SUCCESS;
}

int get_TxIE_bitShift(int timer){
	return 4*timer;
}

int get_TxIF_bitShift(int timer){
	return 4*timer;
}

int get_TxIP_bitShift(int timer){
	return 2;
}

/** \todo IPLxAUTO are unknown.. why?
int get_IPLxAUTO(int priority){
	switch(priority){
		case 1:
			return IPL1AUTO;
		case 2:
			return IPL2AUTO;
		case 3:
			return IPL3AUTO;
		case 4:
			return IPL4AUTO;
		case 5:
			return IPL5AUTO;
		case 6:
			return IPL6AUTO;
	}
}
*/

int set_timerPWM(int OCx, int time_source, int on, float on_time){
	if(OCx<1 || OCx>NUM_SUPPORTED_OUTPUT_COMPARE){
		return ERROR_UNSOPPORTED_INPUT;
	}
	if(time_source!=2 && time_source!=3){
		return ERROR_UNSOPPORTED_INPUT;
	}
	if(on){
		*pOCxCON[OCx-1] &= ~(1<<0); //Clear first bit
		*pOCxCON[OCx-1] |= (6<<0); // set 2nd and 3rd bit for pwm mode;
		if(time_source==2){
			*pOCxCON[OCx-1] &= ~(1<<3);
		}
		else if(time_source==3){
			*pOCxCON[OCx-1] |= (1<<3);
		}
		*pOCxCON[OCx-1] |= (1<<15); 
#if TIMER_DEBUG==1
	printf("set_timerPWM(): PRx=%d, time_source=%d\n", *pPRx[time_source-1], time_source);	
#endif
		*pOCxRS[OCx-1] = *pPRx[time_source-1]*on_time;
	}
	else{
		*pOCxCON[OCx-1] &= ~(1<<15); 
	}
	return SUCCESS;
}

int set_timerPWMRatio(int OCx, int time_source, float on_time){
	if(OCx<1 || OCx>NUM_SUPPORTED_OUTPUT_COMPARE){
		return ERROR_UNSOPPORTED_INPUT;
	}
	if(time_source!=2 && time_source!=3){
		return ERROR_UNSOPPORTED_INPUT;
	}
	*pOCxRS[OCx-1] = *pPRx[time_source-1]*on_time;
	return SUCCESS;
}
