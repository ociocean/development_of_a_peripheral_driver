/* 
 * \file   timer.h
 * \author Ludwig Kragl
 *
 * \date 1 de Novembro de 2017, 10:48
 * \brief Setup timers
 * If you want to simulate the timer behavior on a PC and not on a microcontroller, you need to define SIMULATE 
 * in your program
 */
#ifndef TIMER_H
#define TIMER_H

#define TIMER1 1
#define TIMER2 2
#define TIMER3 3
#define TIMER4 4
#define TIMER5 5

#define OUTCOMP1 1
#define OUTCOMP2 2
#define OUTCOMP3 3
#define OUTCOMP4 4
#define OUTCOMP5 5

#define ON_ 1
#define OFF_ 0

#define PRESCALE_1 1
#define PRESCALE_2 2
#define PRESCALE_4 4
#define PRESCALE_8 8
#define PRESCALE_16 16
#define PRESCALE_32 32
#define PRESCALE_64 64
#define PRESCALE_256 256

#define MAX_SUPPORTED_TIMERS 5
#define SUCCESS 0
#define ERROR_UNSOPPORTED_INPUT -1
#define ERROR_SETTING_NOT_POSSIBLE -2
#define ERROR_32BIT_TIMER_NEEDED -3
#define WARNING_OVERWRITING 1
#ifdef __cplusplus
extern "C" {
#endif
	
#include <stdio.h>
	
	/** \brief Stop a timer.
	 * \param[in] timer  Timer that has to be stopped. (e.g.: 1 for timer1)
	 * \return Errorcode */
	int stop_timer(int timer);
	
	/** \brief Start a timer.
	 * \param[in] timer Timer that has to be started. (e.g.: 1 for timer1)
	  * \return Errorcode */
	int start_timer(int timer);
	
	/** \brief Set the prescaler of a timer
	 * \param[in] timer Timer that has to be set.
	 *  \param[in] prescaler Prescaler that the timer shall have. Supported values: 1, 2, 4, 8, 16, 32, 64, 256.
	 *  \return Error code.*/
	int set_timerPrescaler(int timer, int prescaler);
	
	/** \brief Set period time.
	 * 
	 * Since xc.h implements the PRx register as unsigned int and not as long, the timer can just set as 16
	 * bit timer.
	 * \param[in] timer Timer that has to be set up.
	 * \param[in] period_time Period time [s], that the timer shall have.
	 * \return Error code.*/
	int set_timerPeriod(int timer, double period_time);
	
	/** \brief Set period time.
	 * 
	 * Since xc.h implements the PRx register as unsigned int and not as long, the timer can just set as 16
	 * bit timer.
	 * \param[in] timer Timer that has to be set up.
	 * \param[in] frequency Frequncy [Hz], that the timer shall have.
	 * \return Error code.*/
	int set_timerFrequency(int timer, double frequency);
	
	/** \brief Read the timer value/register .
	 * \warning Function does not cath wrong timer values. -> Wrong timer input will crash your program.
	 * \param[in] timer Timer, that shall be read
	 * \return Timer value, that the timer timer has in the moment */
	unsigned int read_timer(int timer);
	
	/** \brief Enable or disable timer interrupts.
	 * \param[in] timer Timer, that shall bet setup
	 * \param[in] on True, if the interrupts shall be enabled, else false.
	 * \param[in] priority If the timer interrupts will be enabled, set its priority.
	 * \return Error code. */
	int set_timerInterrupt(int timer, int on, int priority);

	/** \brief Enable or disable timers PWM.
	 * \param[in] OCx output compare (pin), that shall be setup
	 * \param[in] timer_source Timer, which shall be the time source of the ouput compare
	 * \param[in] on True, if PWM shall be enabled, also false
	 * \param[in] on_time Ratio of the on off time: on_time = on/(on+off)
	 * \return Error code. */
	int set_timerPWM(int OCx, int time_source, int on, float on_time);
	
	
	/** \brief Sets the on-off ratio of the pwm
	 * \param[in] OCx output compare (pin), that shall be setup
	 * \param[in] timer_source Timer, which is the time source of the ouput compare
	 * \param[in] on_time New ratio of the on off time: on_time = on/(on+off)
	 * \return Error code. */
	int set_timerPWMRatio(int OCx, int time_source, float on_time);
#ifdef __cplusplus
}
#endif

#endif /* TIMER_H */

