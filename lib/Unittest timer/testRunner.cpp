/*
 * testRunner.cpp
 *
 *  Created on: Dec 9, 2012
 *      Author: pf
 */

#include <stdio.h>
 #include <CppUTest/TestHarness.h>

extern "C" {
#define PBUSCLK 40000000L
#include "../timer.h"
#include "p32mx795f512h.h"
}



TEST_GROUP(TimerTest){
	void setup(){
	}

	void teardown(){
	}
};


TEST(TimerTest,Test_stopTimer_ClearRightBit){
	/* Set all bits in T1CON */
	T1CON = 0xFFFF;
	CHECK_EQUAL(stop_timer(1), SUCCESS);
	CHECK_EQUAL(0, T1CONbits.ON);
}

TEST(TimerTest,Test_stopTimer_Timer2){
	/* Set all bits in T1CON */
	T2CON = 0xFFFF;
	CHECK_EQUAL(stop_timer(2), SUCCESS);
	CHECK_EQUAL(0, T2CONbits.ON);
}

TEST(TimerTest,Test_stopTimer_Timer4){
	/* Set all bits in T1CON */
	T4CON = 0xFFFF;
	CHECK_EQUAL(stop_timer(4), SUCCESS);
	CHECK_EQUAL(0, T4CONbits.ON);
}

TEST(TimerTest,Test_stopTimer_Timer5){
	/* Set all bits in T1CON */
	T5CON = 0xFFFF;
	CHECK_EQUAL(stop_timer(5), SUCCESS);
	CHECK_EQUAL(0, T5CONbits.ON);
}

TEST(TimerTest,Test_stopTimer_DontAcceptToSmallValues){
	CHECK_EQUAL(ERROR_UNSOPPORTED_INPUT, stop_timer(0));
}

TEST(TimerTest,Test_stopTimer_DontAcceptToBigValues){
	CHECK_EQUAL(ERROR_UNSOPPORTED_INPUT, stop_timer(99));
}

TEST(TimerTest,Test_startTimer_SetRightBit){
	T1CON = 0x0000;
	CHECK_EQUAL(start_timer(1), SUCCESS);
	CHECK_EQUAL(1, T1CONbits.ON);
}

TEST(TimerTest,Test_setTimerPrescaler_Timer1){
	T1CON = 0x0000;
	CHECK_EQUAL(set_timerPrescaler(1, 4), SUCCESS);
	CHECK_EQUAL(2, T1CONbits.TCKPS);
}

TEST(TimerTest,Test_setTimerPrescaler_Timer2){
	T2CON = 0x0000;
	CHECK_EQUAL(set_timerPrescaler(2, 4), SUCCESS);
	CHECK_EQUAL(2, T2CONbits.TCKPS);
}

TEST(TimerTest,Test_setTimerPrescaler_setTo0){
	T2CON = 0xFFFF;
	CHECK_EQUAL(set_timerPrescaler(2, 1), SUCCESS);
	CHECK_EQUAL(0, T2CONbits.TCKPS);
}

TEST(TimerTest,Test_setTimerPeriod_16bit_ok){
	PR1 = 0x0000;
	int rt = set_timerPeriod(1, 0.25);
	CHECK_EQUAL(SUCCESS, rt);
	CHECK_EQUAL(39062, PR1);
}

TEST(TimerTest,Test_setTimerPeriod_16bit_fail){
	PR1 = 0x0000;
	int rt = set_timerPeriod(1, 0.5);
	CHECK_EQUAL(ERROR_32BIT_TIMER_NEEDED, rt);
	CHECK_EQUAL(0, PR1);
}

/** 32BIT TIMER ARE NOT SOPPORTED: SEE PROBLEMS IN THE DOCUMENTATION.*/ /*
TEST(TimerTest,Test_setTimerPeriod_32bitneeded_but_16bit_available){
	PR1 = 0x0000;
	int rt = set_timerPeriod(1, 1);
	CHECK_EQUAL(ERROR_32BIT_TIMER_NEEDED, rt);
}

TEST(TimerTest,Test_setTimerPeriod_32bit_ok){
	PR2 = 0x0000;
	int rt = set_timerPeriod(2, 1);
	CHECK_EQUAL(SUCCESS, rt);
	printf("Test_setTimerPeriod_32bit_ok: PR2=%d\n", PR2);
	CHECK_EQUAL(39062, PR2);
}*/

TEST(TimerTest,Test_read_timer){
	TMR1 = 0xF1D0;
	CHECK_EQUAL(0xF1D0, read_timer(1));
}

TEST(TimerTest,Test_set_timerInterrupt1){
	IFS0=0xFFFF;
	IPC1=0;
	IEC0=0;
	CHECK_EQUAL(SUCCESS, set_timerInterrupt(1, 1, 6));
	CHECK_EQUAL(0, IFS0bits.T1IF);
	CHECK_EQUAL(6, IPC1bits.T1IP);
	CHECK_EQUAL(1, IEC0bits.T1IE);
}

TEST(TimerTest,Test_set_timerInterrupt2){
	IFS0=0xFFFF;
	IPC2=0;
	IEC0=0;
	CHECK_EQUAL(SUCCESS, set_timerInterrupt(2, 1, 2));
	CHECK_EQUAL(0, IFS0bits.T2IF);
	CHECK_EQUAL(2, IPC2bits.T2IP);
	CHECK_EQUAL(1, IEC0bits.T2IE);
}

TEST(TimerTest,Test_set_timerInterrupt2OFF){
	IFS0=0xFFFF;
	IPC2=0;
	IEC0=0xFFFF;
	CHECK_EQUAL(SUCCESS, set_timerInterrupt(2, 0, 2));
	CHECK_EQUAL(0, IEC0bits.T2IE);
}

TEST(TimerTest,Test_set_PWM){
	PR2=500;
	OC1CON=0;
	CHECK_EQUAL(SUCCESS, set_timerPWM(1, 2, 1, 0.5));
	CHECK_EQUAL(6, OC1CONbits.OCM );
	CHECK_EQUAL(0, OC1CONbits.OCTSEL);
	CHECK_EQUAL(1, OC1CONbits.ON);
	CHECK_EQUAL(250, OC1RS);
}

TEST(TimerTest,Test_set_PWM2){
	PR2=500;
	OC1CON=0xFFFF;
	CHECK_EQUAL(SUCCESS, set_timerPWM(1, 2, 1, 0.5));
	CHECK_EQUAL(6, OC1CONbits.OCM );
	CHECK_EQUAL(0, OC1CONbits.OCTSEL);
	CHECK_EQUAL(1, OC1CONbits.ON);
	CHECK_EQUAL(250, OC1RS);
}

TEST(TimerTest,Test_set_PWM_stop){
	PR2=500;
	OC1CON=0xFFFF;
	CHECK_EQUAL(SUCCESS, set_timerPWM(1, 2, 0, 0.5));
	CHECK_EQUAL(0, OC1CONbits.ON);
}

TEST(TimerTest,Test_set_PWMRatio){
	PR2=500;
	OC1RS=0;
	CHECK_EQUAL(SUCCESS, set_timerPWMRatio(1, 2,  0.5));
	CHECK_EQUAL(250, OC1RS);
}