/** \file   timer_HardwareTest1.c
 * \author Ludwig Kragl
 * \date 4 de Novembro de 2017, 16:11
 */

#include <stdio.h>
#include <stdlib.h>
#define _SUPPRESS_PLIB_WARNING 1
#include <plib.h>

#define SYSCLK 80000000L // System clock frequency, in Hz
#define PBUSCLK 40000000L // Peripheral bus clock

#include <timer.h>

/** \brief 
 */
int main(int argc, char** argv) {
	// Performance optimization (flash access time, enable instruct and data cache,... and PBClock setup
	SYSTEMConfigPerformance(SYSCLK);
	mOSCSetPBDIV(OSC_PB_DIV_2); // This is necessary since SYSTEMConfigPerformance defaults FPBDIV to DIV_1

	TRISD = 0x00;
	PORTD = 0x0;
	set_timerPeriod(2, 200);

	return (EXIT_SUCCESS);
}

