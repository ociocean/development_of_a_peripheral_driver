# Development of a peripheral driver  #

This collection of library shall improve the handling and setting of Microships microcontroller while programming.
Please have a look in the documentation to see more information about how to use.

[Doxygen documentation](https://ludwigkragl.bitbucket.io/development_of_a_peripheral_driver/index.html)

Ludwig Kragl (ludwig.kragl@ua.pt)